package com.backendempleados.backendempleados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendempleadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendempleadosApplication.class, args);
	}

}
