package com.backendempleados.backendempleados.controlador;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backendempleados.backendempleados.dao.EmpleadoDao;
import com.backendempleados.backendempleados.exception.ResourceNotFoundException;
import com.backendempleados.backendempleados.modelo.Empleado;


@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200/")
public class EmpleadoControlador {
    
    @Autowired
    private EmpleadoDao empleadoDao;

    @GetMapping("/empleados")
    public List<Empleado>listarEmpleados(){
        return empleadoDao.findAll();
    }

    @PostMapping("/empleados")
    public Empleado guardarEmpleado(@RequestBody Empleado empleado){
        return empleadoDao.save(empleado);
    }

    @GetMapping("/empleados/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable Long id){
        Empleado empleado = empleadoDao.findById(id)
                            .orElseThrow(()->new ResourceNotFoundException("No se encontro el empleado con el ID :"+ id));
        return ResponseEntity.ok(empleado);                    

    }

    @PutMapping("/empleados/{id}")
    public ResponseEntity<Empleado> putEmpleado(@PathVariable Long id, @RequestBody Empleado actualizarEmpleado){
        Empleado empleado = empleadoDao.findById(id)
                            .orElseThrow(()->new ResourceNotFoundException("No se encontro el empleado con el ID :"+ id));
        
        empleado.setNombre(actualizarEmpleado.getNombre());
        empleado.setApellido(actualizarEmpleado.getApellido());
        empleado.setEmail(actualizarEmpleado.getEmail());
        
        Empleado empleadoActualizado = empleadoDao.save(empleado);
        return ResponseEntity.ok(empleadoActualizado);                    
    }

    @DeleteMapping("/empleados/{id}")
    public ResponseEntity<Map<String,Boolean>>deleteEmpleado(@PathVariable Long id){
        Empleado empleado=empleadoDao.findById(id)
                          .orElseThrow(()->new ResourceNotFoundException("No se encontro el empleado con el ID :"+ id));
        
        empleadoDao.delete(empleado);
        Map<String, Boolean> respuesta = new HashMap<>();
        respuesta.put("eliminar",Boolean.TRUE);
        return ResponseEntity.ok(respuesta);                  
    }
}
