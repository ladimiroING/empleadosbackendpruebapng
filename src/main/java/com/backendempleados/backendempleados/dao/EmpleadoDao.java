package com.backendempleados.backendempleados.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backendempleados.backendempleados.modelo.Empleado;



@Repository
public interface EmpleadoDao extends JpaRepository<Empleado ,Long>{
    
}
